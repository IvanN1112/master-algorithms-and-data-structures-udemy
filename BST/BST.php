<?php
class BinarySearchNode
{
    function __construct($value)
    {
        $this->value = $value;
        $this->left = null;
        $this->right = null;
    }
}

class BinarySearchTree
{
    function __construct()
    {
        $this->root = null;
    }

    function insert($value)
    {
        $newNode = new BinarySearchNode($value);
        if ($this->root == null) {
            $this->root = $newNode;
        } else {
            $curr_node = $this->root;
            while (true) {
                // Left
                if ($value < $curr_node->value) {
                    if (!$curr_node->left) {
                        $curr_node->left = $newNode;
                        return;
                    }
                    $curr_node = $curr_node->left;
                }
                // Right
                else {
                    if (!$curr_node->right) {
                        $curr_node->right = $newNode;
                        return;
                    }
                    $curr_node = $curr_node->right;
                }
            }
        }
    }


    private function findNode($value, $curr_node)
    {
        while ($curr_node) {
            if ($value == $curr_node->value) return $curr_node;
            if ($value < $curr_node->value) {
                $curr_node = $curr_node->left;
            } else {
                $curr_node = $curr_node->right;
            }
        }
    }


    private function removeItem($curr_node, $parent_node)
    {
        //Option 1 - no right child 
        if ($curr_node->right == null) {
            if ($parent_node == null) return $this->root = $curr_node->left;
            ($curr_node->value < $parent_node->value ?
                $parent_node->left = $curr_node->left :
                $parent_node->right = $curr_node->left);
            return;
        }
        //Option 2 - right child of removed element does not have a left child
        if ($curr_node->right->left == null) {
            $curr_node->right->left = $curr_node->left;
            $curr_node->value < $parent_node->value ?
                $parent_node->left = $curr_node->right :
                $parent_node->right = $curr_node->right;

            return;
        }
        //Option 3 - right child that has a left child

        //Find the right child's lefmost item
        $leftmost = $curr_node->right->left;
        $lefmost_parent = $curr_node->right;
        while (isset($leftmost->left)) {
            $lefmost_parent = $leftmost;
            $leftmost = $leftmost->left;
        }
        //Parent's left subtree is now leftmost's right subtree
        $lefmost_parent->left = $leftmost->right;
        $leftmost->left = $curr_node->left;
        $leftmost->right = $curr_node->right;

        if ($parent_node == null) return $this->root = $leftmost;
        $curr_node->value < $parent_node->value ?
            $parent_node->left = $leftmost :
            $parent_node->right = $leftmost;
    }


    private function searchItem($value, $curr_node, $parent_node)
    {
        //Searching for item
        while ($curr_node) {
            if ($value < $curr_node->value) {
                $parent_node = $curr_node;
                $curr_node = $curr_node->left;
            } else if ($value > $curr_node->value) {
                $parent_node = $curr_node;
                $curr_node = $curr_node->right;
            }
            //We got a match!
            else {
                $this->removeItem($curr_node, $parent_node);
                return true;
            }
        }
    }


    function lookup($value)
    {
        $curr_node = $this->root;
        if ($value == $curr_node->value) return $curr_node;

        $result = $this->findNode($value, $curr_node);
        if ($result) return $result->value;

        echo "There is no such node";
    }


    function initiateRemove($value)
    {
        if (!$this->root) {
            echo "Empty tree";
        }

        $curr_node = $this->root;
        $parent_node = null;

        //Searching and removing node
        $this->searchItem($value, $curr_node, $parent_node);
    }

    function breadthFirstSearch()
    {
        $list = [];
        $queue = [];
        $current_node = $this->root;
        $queue[] = $current_node;

        while (!empty($queue)) {
            $current_node = array_shift($queue);
            $list[] = $current_node->value;
            if (isset($current_node->left)) $queue[] = $current_node->left;
            if (isset($current_node->right)) $queue[] = $current_node->right;
        }

        return $list;
    }

    function breadthFirstFind($target)
    {
        $queue = [];
        $current_node = $this->root;
        $queue[] = $current_node;

        while (!empty($queue)) {
            $current_node = array_shift($queue);
            if ($current_node->value == $target) return $current_node->value;
            if (isset($current_node->left)) $queue[] = $current_node->left;
            if (isset($current_node->right)) $queue[] = $current_node->right;
        }

        return "There is no such value";
    }

    function breadthFirstSearchR($queue, $list)
    {
        if (empty($queue)) {
            return $list;
        }
        $current_node = array_shift($queue);
        $list[] = $current_node->value;
        if (isset($current_node->left)) $queue[] = $current_node->left;
        if (isset($current_node->right)) $queue[] = $current_node->right;

        return $this->breadthFirstSearchR($queue, $list);
    }





    function DFSInorder()
    {
        return $this->traverseInorder($this->root, []);
    }





    function DFSPreorder()
    {
        return $this->traversePreorder($this->root, []);
    }






    function DFSPostorder()
    {
        return $this->traversePostorder($this->root, []);
    }




    private function traverseInorder($node, $list)
    {
        if ($node->left !== null) $list = $this->traverseInorder($node->left, $list);
        $list[] = $node->value;
        if ($node->right !== null) $list =  $this->traverseInorder($node->right, $list);

        return $list;
    }
    function inOrderFind($node, $target)
    {

        $stack = [];
        $curr = $node;
        //The top of the stack holds the deepest item
        while (true) {
            //Get to the leftmost item of the stack
            if ($curr !== null) {
                if ($curr->value == $target) return $target;
                $stack[] = $curr;
                $curr = $curr->left;
            }
            //When there are no more left item, pop it and check if it has right child,
            //if it doesn't, we pop the next item, and check for its right children and keep going on until we find the item or the stack is empty
            else if ($stack) {
                $popped = array_pop($stack);
                echo $popped->value . "\n";
                if ($popped->right !== null) $curr = $popped->right;
            } else {
                break;
            }
        }

        return "No matching value.";
    }


    private function traversePreorder($node, $list)
    {
        $list[] = $node->value;
        if ($node->left !== null) $list = $this->traversePreorder($node->left, $list);
        if ($node->right !== null) $list =  $this->traversePreorder($node->right, $list);

        return $list;
    }


    //        9
    //   4        20
    // 1   6   15   170 

    function preOrderFind($node, $target)
    {

        $stack = [$node];
        //The top of the stack holds the deepest item
        while (count($stack) > 0) {
            $popped = array_pop($stack);

            if ($popped->value == $target) return $target;
            print $popped->value . "\n";

            if ($popped->right !== null) $stack[] = $popped->right;
            if ($popped->left !== null) $stack[] = $popped->left;
        }

        return "No matching value.";
    }


    private function traversePostorder($node, $list)
    {
        if ($node->left !== null) $list = $this->traversePostorder($node->left, $list);
        if ($node->right !== null)  $list =  $this->traversePostorder($node->right, $list);
        $list[] = $node->value;

        return $list;
    }
    function postOrderFind($node, $target)
    {
        $stack = [$node];
        $postStack = [];
        //The top of the stack holds the deepest item
        while (count($stack) > 0) {
            $popped = array_pop($stack);
            $postStack[] = $popped;

            if ($popped->left !== null) $stack[] = $popped->left;
            if ($popped->right !== null) $stack[] = $popped->right;
        }
        while (!empty($postStack)) {
            $popped = array_pop($postStack);
            print $popped->value . "\n";
            if ($popped->value == $target) return $target;
        }

        return "There is no such value as $target";
    }
}


$myBST = new BinarySearchTree();
$myBST->insert(9);
$myBST->insert(4);
$myBST->insert(6);
$myBST->insert(20);
$myBST->insert(170);
$myBST->insert(15);
$myBST->insert(1);



// $myBST->initiateRemove(3);
// $myBST->initiateRemove(144);
// $myBST->initiateRemove(212);


// print_r($myBST);


// print_r($myBST->breadthFirstSearch());
// print_r($myBST->breadthFirstSearchR(array($myBST->root), []));
// print_r($myBST->DFSInorder());
// print_r($myBST->DFSPreorder());
// print_r($myBST->DFSPostorder());
// echo $myBST->breadthFirstFind(1010);
echo "The returned in order value is: " . $myBST->inOrderFind($myBST->root, 15) . "\n";
echo "The returned pre order value is: " . $myBST->preOrderFind($myBST->root, 6) . "\n";
echo "The returned post order value is: " . $myBST->postOrderFind($myBST->root, 170) . "\n";
