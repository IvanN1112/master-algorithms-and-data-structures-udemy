<?php
function reverseString($str) {
  //Here we are splitting a string into an array of chars, making empty array with same length as input string.
  //We have a rev value index, rev index start. In the end we are filling the reversed empty string.
  if(!is_string($str) || strlen($str) < 2) {
    return 'there is something wrong, please check your input';
  }
  $split_string = str_split($str);
  $reversedString = array_fill(0,count($split_string),'');
  $revIndex = count($split_string) - 1;
  $revStart= 0;
  while($revIndex >= 0) {
    $reversedString[$revStart] = $split_string[$revIndex];
    $revIndex--;
    $revStart++;
  }
  $reversedString = implode('',$reversedString);
  return $reversedString;
}  
echo reverseString('hello');
