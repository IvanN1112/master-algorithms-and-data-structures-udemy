<?php
$arr1 = [1,2,4,4,16,3,2,6,321,65,12,2,7,42,45,7,63,2,5,6];
//Here we are looking for the recurring character in an array using two different methods. The first one is using n^2 and is comparing each element to all the element from the right of it.

//So using this method we use the array above and start from 1 and search if there is matching value. If there is not we go to the next element and search right from it until we get a match. The maatch we get in this case is 2.This way we would skip any other pairs of elements along the iteration. For example, here we would miss the fact that there are 2 matching fours in the array.
function firstRecurringCharacter($arr) {
  for($i = 0; $i < count($arr);$i++) {
    for($j = $i + 1; $j<count($arr); $j++) 
      if($arr[$i] == $arr[$j]) {
        return $arr[$i];
      }
    }
}



//Here we do it the other way around. We add the items one by one to an array and if we try to add an item that is already there, we return it so we check the items one after another and search for matches. Using $arr1 here we would return 4. 

// Big 0 is (n) but memory usage is also 0(n)

function firstRecurringCharacter2($arr) {
  $map = array();
  for($i=0;$i<count($arr);$i++){ 
    $repeatingValue = in_array($arr[$i],$map);
    if(in_array($arr[$i],$map)) {
      return $arr[$i];
    } else {
      array_push($map,$arr[$i]);
    }
  }
}


echo 'First method result = ' .firstRecurringCharacter($arr1) . ', ';
echo 'Second method result = ' . firstRecurringCharacter2($arr1);
