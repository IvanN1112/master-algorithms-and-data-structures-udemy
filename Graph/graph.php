<?php
class Graph
{
    function __construct()
    {
        $this->number_of_nodes = 0;
        $this->adjacent_list = [];
    }


    function addVertex($node)
    {
        $this->adjacent_list[$node] = [];
    }


    function addEdge($node1, $node2)
    {
        //Undirected graph
        array_push($this->adjacent_list[$node1], $node2);
        array_push($this->adjacent_list[$node2], $node1);
    }
}

$myGraph = new Graph;
$myGraph->addVertex(0);
$myGraph->addVertex(1);
$myGraph->addVertex(2);
$myGraph->addVertex(3);
$myGraph->addVertex(4);
$myGraph->addVertex(5);
$myGraph->addVertex(6);
$myGraph->addEdge(0, 1);
$myGraph->addEdge(0, 2);
$myGraph->addEdge(1, 2);
$myGraph->addEdge(1, 3);
$myGraph->addEdge(4, 3);
$myGraph->addEdge(4, 2);
$myGraph->addEdge(4, 5);
$myGraph->addEdge(5, 6);





print_r($myGraph);
