<?php
class LinkedList {

  public $head;
  public $tail;
  public $count;

  //On construct we will set the head and tail point to the same node
  function __construct($value) {
      $this->head = new Node($value);
      $this->tail = $this->head;
      $this->count = 0;
  }


  //We make new Node,next points to the head and new head becomes the new Node
  function prepend($value) {
    $newHead = new Node($value);
    $newHead->next = $this->head;
    $this->head = $newHead;
    $this->count++;
  }



  //We create a new node, current tail's next points to the new Node and now tail points to new Node. This way the old tail item is before the new one and it still connected to the items before it via the next pointer. 
  function append($value) {
    $newTail = new Node($value);
    //make old tail to point to the new tail
    $this->tail->next = $newTail;
    $this->tail = $newTail;
  }


  //Using a position var we track which item we are on, when curent points to the item in the position we are looking for, we create the new Node with the vallue provided from the function. The new node's next becomes the old item at that position and the previous node's next points to the newly inserted node.
  function insertAt($position,$value) {

    if($position < 0) {
      echo "negative position, please try a positive one\n";
      return;
    }

    if($position == 0) {
      echo "First position chosen, adding item at top of the list. \n";
      $this->prepend($value);
      return;
    }

    $currentIndex = 0;
    $currentNode = $this->head;

    //If chosing an item between the head and the tail.

    while($currentNode->next !== NULL) {

      if($currentIndex == $position){
        $Node = new Node($value);
        $prev->next = $Node;
        $Node->next = $currentNode;
        return;
      }

      if($currentIndex == $position - 1) {
        $prev = $currentNode;
      }


      $currentNode = $currentNode->next;
      $currentIndex++;
    }


    echo "You have chosen to insert as last position or one out of the list scope.\n";
  }


  function remove($position) {
    $arrList = $this->turnIntoArray();
    $itemsCount = count($arrList);
    //Checking for larger than count index
    if($position>$itemsCount) {
      echo 'There is no such position in the list, the highest one is: '. ($itemsCount - 1) . "\n";
      return;
    }
    //Checking for negative value
    if($position < 0) {
      echo 'Invalid index chosen, please chose a non-negative value.';
      return;
    }
    $currentIndex = 0;
    $currentItem = $this->head;
    $prevItem;
    //When deleting first item
    if($position == 0) {
      $this->head = $currentItem->next;
      $currentItem->next = NULL;
      $this->print();
      return;
    }
    //Getting to the item we wish to remove
    while($currentIndex < $position) {
      echo "Item at index: $currentIndex $currentItem->data \n";
      if($currentIndex == $position-1) {
        $prevItem = $currentItem;
      }
      $currentIndex++;
      $currentItem = $currentItem -> next;
    }
    //Point prevItem to one after removed one and remove pointer for removed one
    $prevItem ->next = $currentItem->next;
    $currentItem->next = NULL;
    echo "Removed item was at index: $currentIndex, value is $currentItem->data\n";
    $this->print();
  }


  function search($position) {
    $currentIndex = 0;
    $searchItem = $this->head;
    while($currentIndex < $position) {
      echo "Item at index: $currentIndex $searchItem->data \n";
      $currentIndex++;
      $searchItem = $searchItem -> next;
    }
    return "The item at index $position is: " . $searchItem->data . "\n";
  }


  function reverse() {
    $prev = null;
    $curr = $this->head;
    $next = null;
    $this->head = $this->tail;
    while($curr !== null) {
      $next = $curr->next;
      $curr->next = $prev;
      $prev = $curr;
      $curr = $next;
    }    
    $this->tail = $prev;
  }

  function turnIntoArray() {
    $arr = [];
    $currentNode = $this->head;
    while(isset($currentNode->data)) {
      array_push($arr,$currentNode->data);
      $currentNode = $currentNode->next;
    }
    return $arr;
  }

  //We traverse through each node, via the pointers until we go to the tail and put all of the node datas into a new array that we print in the end.
  function print() {
    $arr = $this->turnIntoArray();
    print_r($arr);
  }
  
}

class Node {
  public $data;
  public $next;
  public function __construct($data) {
    $this->data = $data;
    $this->next = null;
  }
}


$myLinkedList = new LinkedList(10);
$myLinkedList->prepend(20);
$myLinkedList->prepend(30);
$myLinkedList->prepend(40);
$myLinkedList->prepend(50);
$myLinkedList->append(2);
$myLinkedList->insertAt(1,2);
$myLinkedList->insertAt(1,2);
$myLinkedList->insertAt(1,2);
$myLinkedList->insertAt(1,2);
$myLinkedList->insertAt(1,214);
$myLinkedList->insertAt(1,5314);
$myLinkedList->insertAt(0,38);
$myLinkedList->insertAt(0,214);
$myLinkedList->insertAt(0,1211);
$myLinkedList->insertAt(0,2141);
$myLinkedList->insertAt(17,1002023);


print_r($myLinkedList->search(5));

$myLinkedList->print();

$myLinkedList->reverse();

$myLinkedList->print();