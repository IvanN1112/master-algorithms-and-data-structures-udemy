<?php

class Queue
{
    public $items = array();
    public $length, $maxLength;


    function __construct($maxLength)
    {
        $this->length = 0;
        $this->maxLength = $maxLength;
    }


    function enqueue($val)
    {
        if ($this->length == $this->maxLength) {
            echo "Canont add more items, the queue is full \n";
            return;
        }
        array_unshift($this->items, $val);
        $this->length++;
    }


    function dequeue()
    {
        array_pop($this->items);
        $this->length--;
    }


    function first()
    {
        if ($this->isEmpty()) {
            echo "No items in the array";
            return;
        }
        return $this->items[0];
    }


    function isEmpty()
    {
        if (empty($this->items)) {
            return true;
        } else {
            return false;
        }
    }

    function isFull()
    {
        if (count($this->items) == $this->maxLength) {
            return true;
        } else {
            return false;
        }
    }
}



$queue = new Queue(7);
$queue->enqueue(12);
$queue->enqueue(1);
$queue->enqueue(1232);
$queue->enqueue(12);
$queue->enqueue(122143321);
$queue->enqueue(111112);
$queue->enqueue(142);
$queue->enqueue(124);
$queue->enqueue(12);
$queue->enqueue(55412);


print_r($queue);
