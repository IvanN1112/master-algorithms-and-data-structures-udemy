function gradingStudents($grades) {
    // Write your code here
    //We find next multiple of 5 for each grade, if next multiple of 5 minus the grade i     s < 3 we round the grade to that next multiple. We ignore grades that are less than      38.
    for($i = 0; $i < count($grades); $i++) {
        if($grades[$i] < 38) {
          echo $grades[$i];
          continue;
        }
        $nextMultipleOf5 = round(($grades[$i]+5/2)/5)*5;
        if($nextMultipleOf5 - $grades[$i] <3) {
          $grades[$i] = $nextMultipleOf5;
        }
        echo $grades[$i] . "\n";

      }
      return $grades;
}
