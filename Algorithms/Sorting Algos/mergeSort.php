<?php
$numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0, 21, 215, 5163, 62, 7, 6, 27, 357, 3547, 58, 658, 46, 345, 23, 42334, 51, 51224, 3512, 1, 564, 25, 61, 35, 1];

function mergeSort($arr)
{
    $count = count($arr);
    if ($count == 1) {
        return $arr;
    }
    //Split array into left and right;
    $arrCopy = $arr;
    $mid = floor($count / 2);
    $left = array_splice($arr, 0, $mid);
    $right = array_splice($arrCopy, $mid);
    return merge(
        mergeSort($left),
        mergeSort($right)
    );
}


function merge($left, $right)
{
    $result = [];
    $leftI = 0;
    $rightI = 0;


    while ($leftI < count($left) && $rightI < count($right)) {
        if ($left[$leftI] < $right[$rightI]) {
            $result[] = $left[$leftI];
            $leftI++;
        } else {
            $result[] = $right[$rightI];
            $rightI++;
        }
    }

    return
        array_merge($result, array_slice($left, $leftI), array_slice($right, $rightI));
}


print_r(mergeSort($numbers));
