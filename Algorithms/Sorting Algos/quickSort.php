<?php
function quickSortSimple($arr)
{

    $count = count($arr);

    if ($count <= 1) {
        return $arr;
    }
    $pivot = $arr[0];
    $low = $high = [];

    for ($i = 1; $i < $count; $i++) {
        if ($arr[$i] < $pivot) {
            $low[] = $arr[$i];
        } else {
            $high[] = $arr[$i];
        }
    }

    return array_merge(quickSortSimple($low), array($pivot), quickSortSimple($high));
}


function quickSort(&$arr, $leftIndex, $rightIndex)
{
    $index = partition($arr, $leftIndex, $rightIndex);
    //If there are items on the leftIndex of the partitioned index, sort them
    if ($leftIndex < $index - 1) {
        quickSort($arr, $leftIndex, $index - 1);
    }
    //If there are items on the right of the partitioned index, sort them
    if ($index < $rightIndex) {
        quickSort($arr, $index, $rightIndex);
    }
    return $arr;
}

function partition(&$arr, $leftIndex, $rightIndex)
{
    //Pivot is always the middle point, put items smaller than it on its left and bigger on its right
    $pivot = $arr[($leftIndex + $rightIndex) / 2];
    echo "left $leftIndex + right index $rightIndex / 2 = " . ($leftIndex + $rightIndex) / 2 . "\n";

    while ($leftIndex <= $rightIndex) {
        while ($arr[$leftIndex] < $pivot) {
            $leftIndex++;
        }
        while ($arr[$rightIndex] > $pivot) {
            $rightIndex--;
        }
        if ($leftIndex <= $rightIndex) {
            $tmp = $arr[$leftIndex];
            $arr[$leftIndex] = $arr[$rightIndex];
            $arr[$rightIndex] = $tmp;
            $leftIndex++;
            $rightIndex--;
        }
    }
    echo implode(",", $arr) . " @pivot $pivot \n";
    return $leftIndex;
}





// $my_array = [2, 8, 13, 55, 1000, 4, 100, 12, 53, 21, 5, 1254, 124, 134, 1243, 1];
// $my_array = quickSortSimple($my_array);
// print_r($my_array);
$my_array2 = [5, 7, 3, 4, 8, 25];
$my_array2 = quickSort($my_array2, 0, count($my_array2) - 1);
print_r($my_array2);
