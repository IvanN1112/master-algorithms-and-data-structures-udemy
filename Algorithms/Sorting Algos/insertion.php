<?php
$numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0, 14, 5, 135, 1, 12, 34, 215, 12, 21, 4124, 155, 555555, 121, 2412, 4125, 1, 24, 215, 6, 8, 23415, 36, 3156613];

function insertionSort($arr)
{
    $count = count($arr);
    for ($i = 0; $i < $count; $i++) {
        if ($arr[$i] < $arr[0]) {
            array_unshift($arr, array_splice($arr, $i, 1)[0]);
            continue;
        }
        for ($j = 1; $j < $i; $j++) {
            if ($arr[$i] > $arr[$j - 1] && $arr[$i] <= $arr[$j]) array_splice($arr, $j, 0, array_splice($arr, $i, 1)[0]);
        }
    }
    return $arr;
}

print_r(insertionSort($numbers));
