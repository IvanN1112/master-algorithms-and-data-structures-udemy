const sortMeBubble = [1, 4, 1235, 125, 513, 11, 2134, 55, 11, 2143, 11];
const sortMeBubble2 = [
  1, 4, 1235, 125, 513, 11, 2134, 55, 11, 2143, 11, 214, 125, 136, 26, 13, 532,
  61, 34, 56, 56, 1356, 3561, 54, 135, 315, 5613, 631, 632, 3, 132, 413513, 26,
  6247, 232, 4135, 7, 53523, 356, 357, 345, 32373, 42532, 473, 42513, 2734,
  213247, 34, 53, 413, 513, 5123, 12, 4315, 312, 3412, 3, 124, 14, 23, 13,
];

function bubbleSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr.length - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        const temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  console.log(arr);
}

bubbleSort(sortMeBubble);
bubbleSort(sortMeBubble2);
