<?php
$numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0, 15, 566, 124, 222, 6789, 1, 554, 2548, 11, 21];

function selectionSort($arr)
{
    $count = count($arr);
    for ($i = 0; $i < $count; $i++) {

        $min = $i;
        for ($j = $i + 1; $j < $count; $j++) {
            if ($arr[$j] < $arr[$min]) $min = $j;
        }

        $temp = $arr[$i];
        $arr[$i] = $arr[$min];
        $arr[$min] = $temp;
    }
    return $arr;
}

print_r(selectionSort($numbers));
