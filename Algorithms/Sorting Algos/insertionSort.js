function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let j = i - 1;
    let curr = arr[i];
    while (j >= 0 && arr[j] > curr) {
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = curr;
  }
}

const arr = [8, 2, 4125, 136, 1362, 41, 3513, 12, 3, 125, 135, 326, 43753, 0];

insertionSort(arr);

console.log(arr);
