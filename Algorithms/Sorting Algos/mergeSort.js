let sortMe = [1, 8, 9, 2, 5, 4, 3, 9, 8, 1];
let sortMe2 = [
  1, 8, 9, 2, 5, 4, 3, 9, 8, 1, 513, 5132, 61, 321, 41, 1, 64, 21, 61, 6234,
  4513, 12, 0, 51, 15, 1254, 12, 5132,
];

let sortedArray = [];
let counter = 0;

function mergeSort(arr) {
  let count = arr.length;
  if (count === 1) return arr;
  const midpoint = Math.floor(arr.length / 2);
  const left = arr.slice(0, midpoint);
  const right = arr.slice(midpoint, arr.length);
  return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
  let rightI = 0;
  let leftI = 0;
  let result = [];

  while (leftI < left.length && rightI < right.length) {
    if (left[leftI] < right[rightI]) {
      result.push(left[leftI]);
      leftI++;
    } else {
      result.push(right[rightI]);
      rightI++;
    }
  }

  return result.concat(
    left.slice(leftI, left.length),
    right.slice(rightI, right.length)
  );
}

console.log(mergeSort(sortMe));
console.log(mergeSort(sortMe2));
