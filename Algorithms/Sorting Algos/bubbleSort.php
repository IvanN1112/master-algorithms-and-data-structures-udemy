<?php
$numbers = [99, 12, 111, 463, 1325, 77, 2, 44, 6, 2, 1, 5, 63, 121, 5, 112, 567, 3412, 111, 2456, 788, 87, 283, 4, 0];

function bubbleSort($arr)
{
    $count = count($arr);
    for ($i = 0; $i < $count - 1; $i++) {
        for ($j = 0; $j < $count - 1; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }

    return $arr;
}


print_r(bubbleSort($numbers));
