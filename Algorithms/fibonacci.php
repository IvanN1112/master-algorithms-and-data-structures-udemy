<?php
function fibonacciRecursive($n)
{
    if ($n < 2) {
        return $n;
    }
    return fibonacciRecursive($n - 1) + fibonacciRecursive($n - 2);
}


function fibonacciIterative($n)
{
    $arr = [0, 1];
    for ($i = 2; $i <= $n; $i++) {
        $arr[] = $arr[$i - 1] + $arr[$i - 2];
    }
    return $arr[$n];
}


echo fibonacciIterative(6);
