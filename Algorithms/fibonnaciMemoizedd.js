let F = [];

function fib(n) {
  if (n < 2) {
    return n;
  }
  if (F[n] !== -1) return F[n];
  return (F[n] = fib(n - 1) + fib(n - 2));
}

for (let i = 0; i < 100; i++) {
  F[i] = -1;
}

console.log(fib(30));
