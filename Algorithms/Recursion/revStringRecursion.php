<?php
function revString($str)
{
    if (strlen($str) == 0) {
        return '';
    }
    return substr($str, -1) . revString(substr($str, 0, -1));
}


echo revString("Hello world");;
