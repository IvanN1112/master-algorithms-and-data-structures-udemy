<?php

//recursive approach

function factorialRecursive($num)
{
    if ($num == 1) {
        return 1;
    }
    return $num * factorialRecursive($num - 1);
}


//iterative approach

function factorialIterative($num)
{
    $fact = 1;
    for ($i = $num; $i > 0; $i--) {
        $fact *= $i;
    }
    return $fact;
}

echo factorialRecursive(5) . "\n";

echo factorialIterative(5);
