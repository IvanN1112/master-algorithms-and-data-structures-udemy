<?php
function addTo80($n)
{
    echo "calculating...\n";
    return 80 + $n;
}


$cache = [];

function memoizeAddTo80($n)
{
    global $cache;
    if (array_key_exists($n, $cache)) {
        return $cache[$n];
    }
    echo "calculating...\n";
    $cache[$n] = 80 + $n;
    return $cache[$n];
}


// echo addTo80(100) . "\n";
// echo addTo80(100) . "\n";
// echo addTo80(100) . "\n";
// echo addTo80(100) . "\n";
// echo addTo80(100) . "\n";



echo memoizeAddTo80(100) . "\n";
echo memoizeAddTo80(100) . "\n";
echo memoizeAddTo80(100) . "\n";
echo memoizeAddTo80(100) . "\n";
echo memoizeAddTo80(100) . "\n";
