<?php


function fibonacciMemo($n, &$values = [0, 1])
{
    if ($n < 2) {
        return $n;
    }
    if (!array_key_exists($n, $values)) {
        $values[$n] = fibonacciMemo($n - 1, $values) + fibonacciMemo($n - 2, $values);
    }
    return $values[$n];
}


echo fibonacciMemo(10100);
