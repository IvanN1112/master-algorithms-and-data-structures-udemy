class NewArray {
  constructor() {
    this.length = 0;
    this.data = {};
  }

  push(item) {
    this.data[this.length] = item;
    this.length++;
  }

  pop() {
    delete this.data[this.length - 1];
    this.length--;
  }

  get(index) {
    return this.data[index];
  }

  deleteItem(index) {
    const item = this.data[index];
    this.shiftItems(index);
    this.length--;
    return item;
  }

  shiftItems(index) {
    for (let i = index; i < this.length - 1; i++) {
      this.data[i] = this.data[i + 1];
    }
    delete this.data[this.length - 1];
  }
}

const array = new NewArray();
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.push(21312);
array.pop();
array.pop();
array.pop();
array.pop();
array.pop();
array.pop();
array.push("Never Gonna Give You Up");
array.push("Never Gonna Let You Down");
console.log(array.get(8));
console.log(array.get(9));
console.log(array.deleteItem(8));
console.log(array.data);
console.log(array.deleteItem(8));
console.log(array.data);
console.log(array.deleteItem(2));
console.log(array.data);
console.log(array.deleteItem(3));
console.log(array.data);
console.log(array.deleteItem(1));
console.log(array.data);
