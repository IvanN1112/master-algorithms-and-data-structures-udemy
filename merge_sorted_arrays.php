<?php
function mergeSortedArrays($arr1,$arr2,$arr1N,$arr2N,$mergedArray) {
  $i = 0;
  $j = 0;
  $k = 0;
  //Filter elements in 3rd array in ascending order
  while($i<$arr1N && $j < $arr2N) {
    if($arr1[$i] <= $arr2[$j]) {
      $arr3[$k++] = $arr1[$i++];
    } else {
      $arr3[$k++] = $arr2[$j++];
    }
  }
  //Fill with remaining items of either array
  while($i < $arr1N) {
    $arr3[$k++] = $arr1[$i++];
  }
  while($j < $arr2N) {
    $arr3[$k++] = $arr2[$j++];
  }

  return $arr3;
}

$arr1 = [0,4,6,21,124,233,512];
$arr2 = [1,2,3,12,55,210,412,42140,4124215151];
$arr1N = sizeof($arr1);
$arr2N = sizeof($arr2);
$arr3[$arr1N + $arr2N] = array();
print_r(mergeSortedArrays($arr1,$arr2,$arr1N,$arr2N,$arr3));
