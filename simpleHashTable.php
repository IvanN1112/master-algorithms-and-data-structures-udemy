<?php

class HashTable
{

    public $data, $maxLength;

    function __construct($maxLength)
    {
        $this->data = array();
        $this->maxLength = $maxLength;
    }


    function set($key, $value)
    {
        if (count($this->data) < $this->maxLength) {
            $address = crc32($key);
            $this->data[$address] = array($key, $value);
        } else {
            echo "Hash table is full, cannot set item. \n";
        }
    }

    function get($key)
    {
        $address = crc32($key);
        return $this->data[$address][1];
    }

    function remove($key)
    {
        $address = crc32($key);
        unset($this->data[$address]);
    }

    function printKeys()
    {
        foreach ($this->data as $pair) {
            echo "The key is " . $pair[0] . ". \n";
        }
    }

    function countItems()
    {
        $count = 0;
        foreach ($this->data as $item) {
            $count++;
        }
        return $count;
    }
}


$hTable = new HashTable(6);


$hTable->set("monitor", "lg");
$hTable->set("cpu 5ghz", 100);
$hTable->set("ram", "8 gb");
$hTable->set("lock", 6214);
$hTable->set("pc", "lenovo");
$hTable->set("random", 213);
$hTable->set("snap", 6125);

print_r($hTable);

$hTable->remove("pc");
$hTable->set("age", 23);

print_r($hTable);
