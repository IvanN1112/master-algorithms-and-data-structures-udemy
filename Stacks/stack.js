class createStack {
  constructor(maxSize) {
    this.size = 0;
    this.maxSize = maxSize;
    this.top = null;
  }

  isEmpty() {
    if (this.top === null) {
      return true;
    }
    return false;
  }

  peek() {
    return this.top;
  }

  push(val) {
    if (this.size >= this.maxSize) {
      console.log("Max stack size reached, cannot add more items");
      return;
    }
    const newTop = new stackNode(val);
    if (this.isEmpty()) {
      console.log("empty stack, not changing pev");
    } else {
      newTop.link = this.top;
    }
    this.top = newTop;
    this.size++;
  }

  pop() {
    if (this.top === null) {
      return "the list is empty, nothing to pop";
    }
    if (this.top.link !== null) {
      this.top = this.top.link;
    } else {
      this.top = null;
      console.log("All items removed, the list is empty now");
    }
    this.size--;
  }

  search(val) {
    let temp = this.top;
    while (true) {
      if (temp.val === val) {
        return temp.val;
      }
      temp = temp.link;
      if (temp === null) break;
    }
    return "value not present in the stack";
  }
}

class stackNode {
  constructor(val) {
    this.val = val;
    this.link = null;
  }
}

const stack = new createStack(20);

stack.push(12);
stack.push(1214);
stack.push(111);
stack.push(100);
stack.push(10);
stack.push(12);
stack.push(1214);
stack.push(111);
stack.push(100);
stack.push(10);
stack.push(12);

console.log(stack);
