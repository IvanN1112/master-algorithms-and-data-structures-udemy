<?php
class Stack
{

  function __construct()
  {
    $this->data = [];
    $this->top = $this->data;
    $this->bottom = $this->data;
    $this->length = 0;
  }

  function push($value)
  {
    array_push($this->data, $value);
    $this->top = $this->data[count($this->data) - 1];
    $this->bottom = $this->data[0];
    $this->length++;
  }


  function pop()
  {
    //If array is empty return null
    if (empty($this->data)) {
      return null;
    }
    $popped = $this->data[count($this->data) - 1];
    array_pop($this->data);
    //If after item removal is empty set top and bottom to null
    if (empty($this->data)) {
      $this->top = null;
      $this->bottom = null;
      $this->length--;
      return $popped;
    }
    //Otherwise, set top to first array item and bottom to last array item
    $this->top = $this->data[count($this->data) - 1];
    $this->bottom = $this->data[0];
    $this->length--;
    return $popped;
  }

  function peek()
  {
    return $this->top;
  }
}

$myStack = new Stack();
$myStack->push(11);
$myStack->push(12);
$myStack->push(11);
$myStack->push(16);
$myStack->push(111);
$myStack->push(121414);
// $myStack->push(1);
// $myStack->push(125613);
// $myStack->push(123);
// $myStack->push(21);



print_r($myStack);


echo "Popped: " . $myStack->pop() . " new stack is \n";

print_r($myStack);

echo "Peeking...: ";
echo $myStack->peek();

//Comment 2
