<?php
class Stack
{
  function __construct()
  {
    $this->top = null;
    $this->bottom = null;
    $this->length = 0;
  }


  function push($value)
  {
    $node = new Node($value);
    if ($this->bottom == null) {
      $this->top = $node;
      $this->bottom = $node;
    } else {
      $node->next = $this->top;
      $this->top = $node;
    }
    $this->length++;
  }


  function pop()
  {
    //In case there is only item in the stack left
    if ($this->top == $this->bottom) {
      $this->bottom = null;
    }
    $prev = $this->top;
    $this->top = $this->top->next;
    $prev->next = null;
    $this->length--;
  }


  function peek()
  {
    return $this->top->value;
  }
}

class Node
{
  function __construct($value)
  {
    $this->value = $value;
    $this->next = null;
  }
}

$myStack = new Stack();
$myStack->push("google");
$myStack->push("udemy");
$myStack->push("youtube.com");
// $myStack -> push(10);
// $myStack -> push(10);
// $myStack -> pop();
// $myStack -> pop();
// $myStack -> pop();
// $myStack -> pop();
$myStack->pop();

print_r($myStack);
// echo $myStack->peek();
